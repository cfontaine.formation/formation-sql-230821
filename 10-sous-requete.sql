SELECT DISTINCT auteurs.prenom, auteurs.nom FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
INNER JOIN  livres ON livres.id= livre2auteur.id_livre 
WHERE livres.id_genre = (
SELECT genres.id_genre  FROM genres
INNER JOIN livres ON genres.id_genre = livres.id_genre 
GROUP BY genres.id_genre ORDER BY COUNT(livres.id) DESC LIMIT 1  );


SELECT DISTINCT auteurs.prenom, auteurs.nom,genres.nom FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
INNER JOIN  livres ON livres.id= livre2auteur.id_livre
INNER JOIN genres ON livres.id_genre=genres.id_genre
WHERE livres.id_genre IN (
SELECT genres.id_genre  FROM genres
INNER JOIN livres ON genres.id_genre = livres.id_genre 
GROUP BY genres.id_genre HAVING COUNT(livres.id) > 15 );


SELECT titre, annee, concat_ws(' ',prenom,nom)AS auteur FROM (

SELECT titre, annee,auteurs.prenom, auteurs.nom  FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre 
INNER JOIN auteurs ON auteurs.id = livre2auteur.id_auteur WHERE auteurs.deces IS NULL

) AS livre_auteur_deces WHERE titre LIKE 'l%';

 USE meteo;
 

SELECT villes.nom, jour, temp_min FROM villes  
INNER JOIN meteo_data ON meteo_data.ville = villes.id_ville WHERE villes.nom='lille' AND YEAR(jour)=2009 AND temp_min> ALL (

SELECT AVG(temp_matin) FROM meteo_data 
INNER JOIN villes ON meteo_data.ville = villes.id_ville
WHERE YEAR(jour)=2009 AND Villes.nom ='paris'GROUP BY month(jour)

);


SELECT villes.nom, jour, temp_min FROM villes  
INNER JOIN meteo_data ON meteo_data.ville = villes.id_ville WHERE villes.nom='lille' AND YEAR(jour)=2009 AND temp_min> ANY (

SELECT AVG(temp_matin) FROM meteo_data 
INNER JOIN villes ON meteo_data.ville = villes.id_ville
WHERE YEAR(jour)=2009 AND Villes.nom ='paris'GROUP BY month(jour)

);


-- SELECT DISTINCT auteurs.prenom, auteurs.nom,GROUP_CONCAT( genres.nom  SEPARATOR ',') AS genre FROM auteurs 
-- INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
-- INNER JOIN  livres ON livres.id= livre2auteur.id_livre
-- INNER JOIN genres ON livres.id_genre = genres.id_genre GROUP BY auteurs.nom

