
-- jointure interne entre la ville et le pays et on veut afficher le nom de la ville et du pays
SELECT villes.nom , pays.nom FROM 	villes
INNER JOIN pays ON villes.pays = pays.id_pays;

-- Jointure interne entre la ville et le continent et on veut afficher le nom de la ville et du continent
SELECT villes.nom , continents.nom  FROM villes
INNER JOIN continents ON villes.continent = continents.id_continent;

-- jointure interne entre la ville , le pays et le continent et on veut afficher le nom de la ville ,du pays et du continent
select villes.nom as ville, pays.nom as pays, continents.nom as continents
from villes
inner join pays on villes.pays = pays.id_pays
inner join continents on villes.continent = continents.id_continent;

-- requête pour afficher le jour, nom de la ville, le nom du continent , le nom du pays , temp_min
SELECT jour, villes.nom,pays.nom, continents.nom, meteo_data.temp_min FROM villes 
INNER JOIN pays ON villes.pays = pays.id_pays
INNER JOIN continents ON villes.continent=continents.id_continent
INNER JOIN meteo_data ON villes.id_ville=meteo_data.ville

-- requête pour afficher le nom de la ville, le nom du continent , le nom du pays , temp_minimum pour le 16/10/2010 pour Paris
SELECT villes.nom,pays.nom, continents.nom,meteo_data.temp_min FROM villes 
INNER JOIN pays ON villes.pays = pays.id_pays
INNER JOIN continents ON villes.continent=continents.id_continent
INNER JOIN meteo_data ON villes.id_ville=meteo_data.ville
WHERE villes.nom='paris' AND jour='2010-10-16';

-- requête pour afficher le nom de la ville, le nom du pays, la moyenne des temp_minimum pour le mois juillet 2013 à Nantes
SELECT villes.nom,pays.nom, AVG(meteo_data.temp_min) FROM villes 
INNER JOIN pays ON villes.pays = pays.id_pays
INNER JOIN meteo_data ON villes.id_ville=meteo_data.ville
WHERE villes.nom='nantes' AND YEAR(jour)=2013 AND MONTH(jour)=7;

-- requête pour afficher le nom de la ville, le nom du pays, la moyenne des temp_minimum pour tous les mois de 2013 à Nantes
SELECT MONTHNAME(jour) as mois,villes.nom AS Ville,pays.nom AS Pays, ROUND(AVG(meteo_data.temp_min),1) AS 'moyennne temperature minimum' FROM villes 
INNER JOIN pays ON villes.pays = pays.id_pays
INNER JOIN meteo_data ON villes.id_ville=meteo_data.ville
WHERE villes.nom='nantes' AND YEAR(jour)=2013 GROUP BY MONTH(jour);

