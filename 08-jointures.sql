-- Jointure interne
SELECT livres.id,titre, annee,livres.genre, genres.id, genres.nom FROM livres,genres WHERE livres.genre = genres.id;

SELECT titre, annee, genres.nom FROM livres,genres WHERE livres.genre = genres.id;

SELECT titre,annee, nom FROM livres INNER JOIN genres ON livres.genre =genres.id WHERE annee>1970 AND nom='Policier';

SELECT titre, annee,genres.nom, auteurs.prenom, auteurs.nom FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id = livre2auteur.id_auteur 
INNER JOIN livres ON livres.id = livre2auteur.id_livre
INNER JOIN  genres ON livres.genre = genres.id;

-- produit catésien
SELECT livres.id,titre, annee,livres.genre, genres.id, genres.nom FROM livres,genres

SELECT * FROM livres CROSS JOIN genres;

-- Jointure externe
SELECT genres.nom , livres.titre , livres.annee FROM genres LEFT JOIN  livres ON genres.id = livres.genre;
SELECT genres.nom , livres.titre , livres.annee FROM genres LEFT JOIN  livres ON genres.id = livres.genre WHERE titre IS NULL;

SELECT genres.nom , livres.titre , livres.annee FROM livres RIGHT JOIN  genres ON genres.id = livres.genre;
SELECT genres.nom , livres.titre , livres.annee FROM livres RIGHT JOIN  genres ON genres.id = livres.genre WHERE titre IS NULL;

SELECT * FROM livres l FULL JOIN genres g  ON g.id_genre = l.id_genre;

-- Joiture naturel
 SELECT * FROM livres NATURAL JOIN genres
 
 SELECT * FROM livres INNER JOIN genres ON livres.id_genre =genres.id_genre
 
 -- auto jointure
 CREATE TABLE IF NOT EXISTS employes
 ( 
 	id INTEGER PRIMARY KEY AUTO_INCREMENT,
 	prenom VARCHAR(50) NOT NULL,
 	nom VARCHAR(50) NOT NULL,
 	manager INTEGER,
 	constraint fk_manager 
foreign key (manager)
references employes(id)
 );

 INSERT INTO employes(prenom, nom , manager) VALUES
 ('aaaaaa','aaaaaaa', NULL),
('bbbbbba','bbbbbb', 1),
('ccccc','ccccc', NULL),
 ('bbbbbba','ffffffffff', 1),
 ('ccccccccc','ffffffffff', 3);

SELECT emp.prenom, emp.nom, CONCAT_WS(' ',ma.prenom ,ma.nom) AS nom_manager FROM employes AS emp
LEFT JOIN employes AS ma ON ma.id = emp.manager;


 