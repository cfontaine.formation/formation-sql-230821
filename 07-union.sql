
CREATE TABLE production_france
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	date_production DATE,
	nom_produit VARCHAR(60),
	quantite INTEGER,
	conditionnement VARCHAR(60)
);

CREATE TABLE production_espagne
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	date_production DATE,
	nom_produit VARCHAR(60),
	quantite INTEGER,
	conditionnement VARCHAR(60)
);

INSERT INTO production_france(date_production,nom_produit,quantite,conditionnement) VALUE
('2021-08-24','amidon',20,'sac papier'),
('2021-08-24','produit A',50,'sac papier'),
('2021-08-24','produit B',5,'bigbag'),
('2021-08-25','produit C',4,'tonneau'),
('2021-08-25','produit A',3,'bigbag');

INSERT INTO production_espagne(date_production,nom_produit,quantite,conditionnement) VALUE
('2021-08-24','amidon',20,'sac papier'),
('2021-08-24','produit C',50,'bigbag'),
('2021-08-25','produit B',4,'tonneau'),
('2021-08-25','produit D',3,'bigbag');

SELECT date_production,nom_produit,quantite FROM  production_france
UNION
SELECT date_production,nom_produit,quantite FROM  production_espagne

SELECT date_production,nom_produit,quantite FROM  production_france
UNION ALL
SELECT date_production,nom_produit,quantite FROM  production_espagne