-- Supprimer la base de données exemple si elle existe
DROP DATABASE IF EXISTS exemple;

-- Créer une base de données exemple
CREATE DATABASE exemple;

-- Choisir exemple comme base de données courante 
USE exemple;

-- Créer une table articles qui contient 3 colonnes reference, description et prix
CREATE TABLE articles (
  reference BIGINT,
  description VARCHAR(255),
  prix DECIMAL(6,2)  # 6 chiffres au total et 2 chiffres après la virgule max=9999.99 min=-9999.99
  	
);

-- Exercice: Gestion de vols
CREATE TABLE vol(
numero_vols BIGINT,
heure_depart DATETIME,
heure_arrivee DATETIME,
ville_depart VARCHAR(50),
ville_arrivee VARCHAR(50)
);

CREATE TABLE avions(
numero_avion INTEGER,
modele VARCHAR(40),
capacite SMALLINT
);

CREATE TABLE pilotes(
numero_pilote INTEGER,
pr VARCHAR(40),
nom VARCHAR(50),
nombre_heure_vol INTEGER
);

/* Modification de table */
-- Renommer une table
RENAME TABLE vol TO vols;

-- Ajouter une colonne à une table
ALTER TABLE pilotes ADD age SMALLINT;

-- Ajouter une contrainte: Clé primaire à la table vols
ALTER TABLE vols ADD
CONSTRAINT pk_vols PRIMARY KEY (numero_vols);

-- Ajouter une contrainte: Clé primaire à la table avions
ALTER TABLE avions ADD
CONSTRAINT pk_avions PRIMARY KEY (numero_avion);

-- Ajouter une contrainte: Clé primaire à la table pilotes
ALTER TABLE pilotes ADD
CONSTRAINT pk_pilotes PRIMARY KEY (numero_pilote);

-- Renommer une colonne
ALTER TABLE pilotes CHANGE pr prenom VARCHAR(40);

-- Modifier le type d’une colonne
ALTER TABLE pilotes MODIFY prenom VARCHAR(50);

-- Supprimer une contrainte
-- ALTER TABLE pilotes DROP CONSTRAINT PRIMARY KEY;

-- Modifer le type de la colonne ville_depart
ALTER TABLE vols
MODIFY ville_depart VARCHAR(50) NOT NULL DEFAULT 'Paris';

ALTER TABLE pilotes
MODIFY numero_pilote INTEGER AUTO_INCREMENT;

/* Relation entre les tables */
-- Relation 1-n

-- Ajouter une colonne de pilote à la table vols
ALTER TABLE vols ADD pilote INTEGER;

-- Ajouter une contrainte clé étrangère fk_pilote sur la colonne pilote dans la table vols 
-- qui fait référence à la clé primaire numero_pilote de la table pilotes
ALTER TABLE vols ADD
CONSTRAINT fk_pilote
FOREIGN KEY (pilote)
REFERENCES pilotes(numero_pilote);

-- Ajouter une colonne de avion dans la table vols
ALTER TABLE vols ADD avion INTEGER;

-- Ajouter une contrainte clé étrangère fk_avion sur la colonne avion dans la table vols 
-- qui fait référence à la clé primaire numero_avions de la table avions
ALTER TABLE vols ADD
CONSTRAINT fk_avion
FOREIGN KEY (avion)
REFERENCES avions(numero_avion);

-- Relation n-n
CREATE TABLE films(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	titre VARCHAR(100) NOT NULL
);

CREATE TABLE genres(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL
);

-- Création d'un table de jointure films_genres qui permettera de réaliser 
-- la relation n-n entre films et genres 
CREATE TABLE films_genres
(
	id_film INTEGER ,
	id_genre INTEGER, 
	
	-- Contraintes de clé étrangère entre la table films et films_genres
	CONSTRAINT fk_films
	FOREIGN KEY (id_film)
	REFERENCES films(id),
	
	-- Contrainte de clé étrangère entre la table genres et films_genres
	CONSTRAINT fk_genres
	FOREIGN KEY (id_genre)
	REFERENCES genres(id),
	
	-- Contrainte de clé primaire pour la table films_genres
	-- la clé primaire est composée des colonnes id_film, id_genre
	CONSTRAINT pk_films_genres
	PRIMARY KEY (id_film,id_genre)
);

-- Supprimer la contrainte fk_films et fk_genres
ALTER TABLE films_genres
DROP CONSTRAINT fk_films;

ALTER TABLE films_genres
DROP CONSTRAINT fk_genres;

/*Ajouter une containte de clé étrangère avec supression en cascade entre 
 films_genres et films et films_genres et genres
 
 Lorsque supprime un films les lignes qui lui sont associées dans films_genres sont supprimées
*/ 
ALTER TABLE films_genres ADD
CONSTRAINT fk_films
FOREIGN KEY (id_film)
REFERENCES films(id) ON DELETE CASCADE;

-- Idem pour les genres
ALTER TABLE films_genres ADD
CONSTRAINT fk_genres
FOREIGN KEY (id_genre)
REFERENCES genres(id) ON DELETE CASCADE;

-- Ajouter un INDEX sur les colonnes nom et prénom de pilotes
CREATE INDEX inom_pilote ON pilotes (nom,prenom); 
-- Supprimer l'index inom_pilote
DROP INDEX inom_pilote ON pilotes;

