-- requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT CONCAT_WS('-',LEFT(nom,3),'0000',LENGTH(nom)) AS code_ville FROM villes;
SELECT CONCAT(LEFT(nom,3),'-0000-',LENGTH(nom)) AS code_ville FROM villes;

-- requête pour trouver la température du matin maximal pour l'année 2010
SELECT max(temp_matin) FROM meteo_data WHERE YEAR(jour) = 2010;

-- requête afficher la moyenne de température du matin pour l'année 2018 pour Paris
SELECT TRUNCATE(avg(temp_matin),1) FROM meteo_data  WHERE  YEAR(jour) = 2018 AND ville=2;
