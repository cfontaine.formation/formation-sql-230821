CREATE VIEW v_auteur_vivant AS SELECT prenom , nom ,naissance, nation
FROM auteurs
WHERE deces IS  NULL ;  
SHOW TABLES; 

SELECT * FROM v_auteur_vivant; 

DROP VIEW v_auteur_vivant;


CALL proc1();

SHOW PROCEDURE STATUS;

DROP PROCEDURE proc1;

SET @var1='test';
SELECT @var1;

CALL proc2();
-- SELECT var_local;

CALL proc3( 12);

CALL proc4(@var1);
SELECT @var1;

CALL proc5(15,@var1);
SELECT @var1;

CALL proc6(1,@var1);
SELECT @var1;

CALL proc7(@var1);
SELECT @var1;
