-- Sélectionner des données
USE bibliotheque;

SELECT prenom,nom FROM auteurs;

-- Quand il y a une ambiguité sur le nom d'une colonne, on ajoute le nom de la table  table.colonne
-- pour différentier les 2 colonnes
SELECT prenom,auteurs.nom,pays.nom FROM auteurs,pays;

-- On peut mettre dans les colonnes d'un SELECT une constante ou une colonne qui provient d'un calcul
SELECT titre,annee,'age',2021-annee FROM livres;

-- AS permet de spécifier un Alias pour le nom d'un colonne ou d"une table
SELECT titre AS titre_livre,annee,2021-annee AS age FROM livres;

SELECT prenom,auteurs.nom AS nom_auteurs,pays.nom AS pays FROM auteurs,pays;

SELECT titre,annee,2021-annee AS age FROM livres AS book;

-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous le titres de livre qui sont sortie après 1970
SELECT titre FROM livres WHERE annee>1970;

-- Selection des titres, de l'année de sortie du livre et l'age de livre
-- des livres qui ont un age inférieur à 20 ans
SELECT titre ,annee,2021-annee AS age FROM livres WHERE 2021-annee<20;

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1920 et 1950
SELECT titre,annee FROM livres WHERE annee>1920 AND annee<1950;

-- Selection des titres, de l'année de sortie du livre qui sont sorties en 1923 et en 1964
SELECT titre,annee FROM livres WHERE annee=1923 OR annee=1964;

-- Selection des titres, de l'année de sortie du livre qui sont sorties aprés 1975 ou avant 1950
SELECT titre,annee FROM livres WHERE annee>1975 OR annee<1920;

-- L'opérateur NOT inverse le resultat
-- Selection des titres, de l'année de sortie du livre qui sont sorties avant 1980 (Condition inversée)
SELECT titre,annee FROM livres WHERE NOT annee>1980; 

-- Selection du prénom, du nom et de la date de naissance pour les auteurs qui sont né avant le 1er janvier 1930
-- et qui ont pour prénom Pierre ou James
SELECT prenom, nom, naissance FROM auteurs WHERE (prenom='Pierre' OR prenom='James') AND naissance <'1930-01-01';

-- Sélection du titre et de l'année pour les livres sortie en 1954,1965 ou 1978
SELECT titre, annee FROM livres WHERE annee IN (1954,1965,1978);

-- Sélection du titre et de l'année pour les livres sortie entre 1954 et 1965
SELECT titre, annee FROM livres WHERE annee BETWEEN 1954 AND 1965; 

-- IS NULL permet de tester si une valeur est égal à NULL
-- IS NOT NULL  permet de tester si une valeur est différente de NULL
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui ne sont pas décédés => deces = NULL 
SELECT * FROM auteurs WHERE deces IS NULL; 

-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui ne sont  décédés
SELECT * FROM auteurs WHERE deces IS NOT NULL;

-- Avec LIKE % représente 0,1 ou plusieurs caratères inconnues
--           _ représente un caratère inconnue
-- Sélection de tous les colonnes de la table livres pour les livres dont le titre commence par un D (ne tient pas compte de la casse)
SELECT * FROM livres WHERE titre LIKE 'D%';

-- Sélection de tous les colonnes de la table livres pour les livres dont le titre 
-- de 4 caractère qui commence par un D
SELECT * FROM livres WHERE titre LIKE 'D___';

SELECT * FROM livres WHERE titre LIKE '_u%';

-- Selection de tous les livres triés par rapport à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY titre;

-- Selection de tous les livres triés par rapport à leur titre par l'inverse de l'ordre alphabétique
SELECT * FROM livres ORDER BY titre DESC;

-- Selection de tous les livres triés par rapport à leur année  de sortie (ordre décroissant)et à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY annee DESC ,titre;

-- Sélection des 5 livres les plus anciens
SELECT * FROM livres ORDER BY annee  LIMIT 5 ;

-- Sélection 5 livres à partir du quatrième 
SELECT * FROM livres LIMIT 5 OFFSET 4;
-- idem autre syntaxe (MySQL)
SELECT * FROM livres LIMIT 4,5;