-- Fonction arithmétique
SELECT ceil(1.4) AS ceil, floor(1.4) AS floor, round(1.3338888,3) AS round, truncate(1.33888888,3);

SELECT id, titre ,annee FROM livres ORDER BY rand() LIMIT 5;

-- Fonctions chaînes de caractères
SELECT LENGTH (titre) AS nb_caractere_titre FROM livres;

SELECT CONCAT(prenom, ' ', nom,'-',id) AS ident_auteur FROM auteurs;

SELECT CONCAT_WS(' ',prenom, nom,id) AS ident_auteur FROM auteurs;

SELECT format(1000000045,4); 

SELECT INSERT(prenom, 2,0,nom) FROM auteurs;

SELECT nom, POSITION('cr' IN nom) FROM auteurs;

SELECT REVERSE(nom) FROM auteurs;

SELECT REPLACE(nom,'cr','----') FROM auteurs;

SELECT REPEAT('a',15);

SELECT titre, LEFT(titre,3), RIGHT(titre,3) FROM livres; 

SELECT titre, STRCMP(titre,'dune') FROM livres; 

SELECT substr(titre,3,4) FROM livres;

SELECT upper(titre) FROM livres;

SELECT TRIM('   az ert      '); # donne 'az ert'

-- Fonctions de date
SELECT NOW() FROM livres;

SELECT datediff('2021-12-31',current_date());

SELECT date_add(current_date(),INTERVAL 4 year) ;

SELECT date_sub(now(),INTERVAL 15 hour) ;

SELECT last_day(now());

SELECT dayname(now());

SELECT dayofmonth(now());

-- Fonctions d’agrégation
SELECT MAX(annee) FROM livres;

SELECT MIN(titre) FROM livres;

SELECT AVG(annee) FROM livres;

SELECT COUNT(id) AS nb_livre FROM livres WHERE annee BETWEEN 1950 AND 1960;

SELECT COUNT(DISTINCT annee) AS nb_livre FROM livres WHERE annee BETWEEN 1950 AND 1960; 

-- Autre fonctions
SELECT id, BIN(id), HEX(id),  OCT(id) FROM livres;

SELECT CURRENT_USER() AS utilisateur,DATABASE() AS base_de_donnee, VERSION() AS version;

SELECT COALESCE(NULL, 1, 2,NULL, 'Hello');

SELECT NULLIF(5, "Hello");	# 5

SELECT NULLIF("Hello", "Hello"); # NULL

-- Regroupement

SELECT annee, COUNT(id) AS nb_livres FROM livres WHERE annee BETWEEN 1950 AND 1959 GROUP BY annee;

SELECT annee, COUNT(id) AS nb_livres FROM livres WHERE annee BETWEEN 1950 AND 1959 GROUP BY annee HAVING nb_livres>1;

SELECT genre , COUNT(id) AS nb_genre FROM livres GROUP BY genre; 
