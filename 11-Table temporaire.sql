USE exemple;
CREATE TEMPORARY TABLE clients
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (255)
) ;

SHOW tables;

INSERT INTO clients(nom)VALUES ('John Doe');

SELECT * FROM clients;
USE bibliotheque;
-- CTE
WITH auteur_vivant AS (
SELECT prenom , nom ,naissance, nation
FROM auteurs
WHERE deces IS NOT NULL ),

 auteur_vivant_usa AS (
SELECT prenom , nom ,naissance
FROM auteur_vivant 
WHERE nation=1 )

SELECT * FROM auteur_vivant_usa  WHERE naissance > '1930-01-01';

-- SELECT * FROM auteur_vivant WHERE naissance > '1930-01-01';

CREATE TABLE  auteurs_vivant 
SELECT prenom , nom ,naissance, nation
FROM auteurs
WHERE deces IS NOT NULL;