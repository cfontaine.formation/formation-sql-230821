-- Exercice pizzeria
DROP DATABASE IF EXISTS pizzeria;
CREATE DATABASE pizzeria;
USE pizzeria;

CREATE TABLE ingredients
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL DEFAULT ''
);

CREATE TABLE pizzas
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	prix DECIMAL(4,2) NOT NULL DEFAULT 12.0
);

CREATE TABLE pizzas_ingredients
(
	id_pizza INTEGER,
	id_ingredient INTEGER,

	CONSTRAINT fk_pizza_pizzas_ingredients
	FOREIGN KEY (id_pizza)
	REFERENCES pizzas(id),
	
	CONSTRAINT fk_ingredients_pizzas_ingredients
	FOREIGN KEY (id_ingredient)
	REFERENCES ingredients(id),
	
	CONSTRAINT pk_pizzas_ingredients
	PRIMARY KEY(id_pizza,id_ingredient)
);

CREATE TABLE clients
(
	numero_client INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	adresse VARCHAR(255) NOT NULL
);

CREATE TABLE commandes 
(
	numero_commande INTEGER PRIMARY KEY AUTO_INCREMENT,
	heure_commande DATETIME,
	heure_livraison DATETIME,
	client INTEGER,
	
	CONSTRAINT fk_clients_commandes
	FOREIGN KEY (client)
	REFERENCES clients(numero_client)
);

CREATE TABLE commandes_pizzas
(
	id_commande INTEGER,
	id_pizza INTEGER,
	quantite SMALLINT NOT NULL DEFAULT 1,
	
	CONSTRAINT fk_pizza_commandes_pizzas
	FOREIGN KEY (id_pizza)
	REFERENCES pizzas(id),
	
	CONSTRAINT fk_commandes_commandes_pizzas
	FOREIGN KEY (id_commande)
	REFERENCES commandes(numero_commande),
	
	CONSTRAINT pk_commandes_pizzas
	PRIMARY KEY(id_commande,id_pizza)
);

ALTER TABLE pizzas
ADD photo MEDIUMBLOB;

-- Insertion de données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO ingredients VALUES (1,'Champignon');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO ingredients(nom) VALUES ('Jambon'); # comme prix n'est pas spécifié il prendra la valeur par défaut 12.0
												# idem pour photo qui prendra la valeur NULL
INSERT INTO pizzas (nom) VALUES ('Forrestière');
INSERT INTO pizzas (nom,prix) VALUES ('pizza saumon',16.5);

-- Insérer plusieurs lignes à la fois
INSERT INTO pizzas (nom,prix)
VALUES ('pizza sicilien',16.5),
('royale',13.5),
('maghareta',12.0);

-- LOAD_FILE permet de charger l'image pizza.jpg dans photo qui de type BLOB (données binaires)
INSERT INTO pizzas (nom,prix,photo) VALUES ('pizza Blanche',16.5,LOAD_FILE('C:/Formations/pizza.jpg'));

-- Supprimer des données
-- Supression de la ligne qui a pour id 6 dans la table pizzas
DELETE FROM pizzas WHERE id=6;

-- Supression de toutes les lignes qui ont un prix supérieur à 14.0 dans la table pizzas
DELETE FROM pizzas WHERE prix > 14.0; 

-- Supression de toutes les lignes de la table pizzas => ne remet pas les colonnes AUTO_INCREMENT à 0
DELETE FROM pizzas;

-- On ne peut pas faire de TRUNCATE s'il y a une contrainte avec une clé étrangère
-- TRUNCATE TABLE pizzas; # TABLE est optionnel
CREATE TABLE livreurs
( 
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40)
);

INSERT INTO livreurs(nom) VALUES ('durant'),('dupont');

-- Supression de toutes les lignes de la table pizzas => remet à 0   les colonnes AUTO_INCREMENT
TRUNCATE livreurs;

INSERT INTO livreurs(nom) VALUES ('Lucas');

INSERT INTO clients (nom,adresse) VALUES ('Durant','1, rue esquermoise');
TRUNCATE TABLE clients;

-- Modifier des données
-- Modification de la colonne photo pour la ligne qui a pour id 8 => nouvelle valeur pizza.jpg
UPDATE pizzas 
SET photo=LOAD_FILE('C:/Formations/pizza.jpg')
WHERE id=8;

-- Modification de toutes les lignes de table pizzas qui ont un prix <14 => nouvelle valeur 15.0
UPDATE pizzas 
SET prix=15.0
WHERE prix<14.0;

-- Modification de toutes les lignes de table pizzas, On ajoute 2 aux prix de toutes les pizzas 
UPDATE pizzas 
SET prix=prix+2;
