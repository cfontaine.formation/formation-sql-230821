-- requête pour trouver les villes qui sont en Europe
SELECT * FROM continents; # id  Europe -> 1
SELECT nom FROM villes WHERE continent=1;

-- requête pour trouver les villes dont le nom commence par C
SELECT nom  FROM villes WHERE nom LIKE'c%';

-- requête pour avoir les infos météorologique pour Berlin id=10
SELECT * FROM meteo_data WHERE ville=10;

-- requête pour avoir les infos météorologique  Le 1 janvier 2011 pour Berlin id=10
SELECT * FROM meteo_data WHERE ville=10 AND jour='2011-01-01';

-- requête pour avoir les infos météorologique entre Le 1 janvier 2011 et 3 mars 2011  pour  Lille id=1
SELECT * FROM meteo_data WHERE ville=1 AND jour BETWEEN '2011-01-01' AND '2011-03-01';

-- requête pour trouver toutes les villes qui ne sont pas en Europe
SELECT nom FROM villes WHERE NOT continent=1;

-- requête pour avoir  pour Paris id=2, l'info météorologique pour Le 1 janvier 2011 , le 3 mars 2009 et le 25 décembre 2010
SELECT * FROM meteo_data WHERE ville=2 AND jour IN ('2011-01-01','2009-03-01','2010-12-25');

-- requête pour trouver les villes qui sont en Europe ou bien qui sont en Afrique
SELECT nom FROM villes WHERE continent=1 OR continent=4;

-- Écrire la requête pour calculer la température moyenne  pour Lille pour tous les jours de mars 2013 on nommera la colonne temp_moy ,on affichera le jour , la pression et le temp_moy trier les résultat pression + grand à + petite
SELECT jour, pression,(temp_matin + temp_midi + temp_apres_midi)/3 AS temp_moy FROM meteo_data WHERE ville=1 AND jour BETWEEN '2013-03-01' AND '2013-03-31' ORDER BY pression DESC;